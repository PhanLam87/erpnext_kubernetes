#!/bin/bash

if [ "$1" = 'start' ]; then

  su frappe -c "node /home/frappe/frappe-bench/apps/frappe/socketio.js"

elif [ "$1" = 'doctor' ]; then

  su frappe -c "node /home/frappe/frappe-bench/apps/frappe/health.js"

else

  exec su frappe -c "$@"

fi
