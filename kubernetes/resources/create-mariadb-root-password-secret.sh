#!/bin/bash
set -e

if [[ -z "$DB_ROOT_PASSWORD" ]]; then
    echo "DB_ROOT_PASSWORD is not set"
    exit 1
fi

export BASE64_PASSWORD=$(echo $DB_ROOT_PASSWORD | base64)

envsubst '${BASE64_PASSWORD}' \
    < ./mariadbrootpasswordsecret.yaml.template > ./mariadbrootpasswordsecret.yaml
