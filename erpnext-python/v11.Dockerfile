FROM bitnami/python:latest-prod

RUN useradd -ms /bin/bash frappe
WORKDIR /home/frappe/frappe-bench
RUN install_packages \
    git \
    wkhtmltopdf \
    mariadb-client \
    gettext-base

RUN mkdir -p apps logs commands

RUN virtualenv env \
    && . env/bin/activate \
    && cd apps \
    && git clone --depth 1 -o upstream https://github.com/frappe/frappe --branch version-11 \
    && git clone --depth 1 -o upstream https://github.com/frappe/erpnext --branch version-11 \
    && pip3 install --no-cache-dir -e /home/frappe/frappe-bench/apps/frappe \
    && pip3 install --no-cache-dir -e /home/frappe/frappe-bench/apps/erpnext

COPY ./commands/* /home/frappe/frappe-bench/commands/
COPY ./common_site_config.json.template /opt/frappe/common_site_config.json.template

# Setup docker-entrypoint
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh / # backwards compat

WORKDIR /home/frappe/frappe-bench/sites

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
