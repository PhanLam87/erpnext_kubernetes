# Decoupled Docker images Frappe / ERPNext

ERPNext images are Python 3 only. For SocketIO NodeJS used is latest stable.
For built assets NodeJS is tagged to v10 for v11 and v12 for v12.

### ERPNext Assets

nginx image to serve assets and reverse proxy to services
On container entry it copies asset files to /assets directory.
These assets are required by python image.

### EPRNext Python

- This image has erpnext and frappe installed as python app
- Serves the frappe/erpnext app via gunicorn
- Runs workers with `command: worker`,
- For worker set `WORKER_TYPE` as `default`, `long` or `short`, e.g `WORKER_TYPE=default`
- command: `docker-entrypoint.sh new` creates new site as per ENV variables
- command: `docker-entrypoint.sh migrate` migrates site(s) as per ENV variable
- command: `docker-entrypoint.sh doctor` checks bench status
- command: `docker-entrypoint.sh backup` backs up site(s) as per ENV variable

### Frappe SocketIO

This image is used to serve frappe socketio
`health.js` is added for container health checks

# Getting Started

Clone the repo and change working directory

```sh
git clone https://gitlab.com/revant.one/erpnext_kubernetes && cd erpnext_kubernetes
```

Copy `env-example` to `.env`

```sh
cp env-example .env
```

Start services

```sh
docker-compose up
```

Notes:
- Wait for mariadb to start successfully
- Update docker-compose for nginx ports. default exposed port is 80

## Create new site

`INSTALL_ERPNEXT` is optional. By default script will only add a frappe site.

```sh
docker exec -it \
    -e SITE_NAME=test.localhost \
    -e DB_ROOT_USER=root \
    -e DB_ROOT_PASSWORD=admin \
    -e ADMIN_PASSWORD=admin \
    -e INSTALL_ERPNEXT=1 \
    erpnext_kubernetes_erpnext-python_1 docker-entrypoint.sh new
```

Visit url site url (http://test.localhost) from browser to complete setup wizard

## Migrate site(s)

`SITES` env variable is list of sites separated by `:` to migrate. e.g. `SITES=site1.localhost` or `SITES=site1.localhost:site2.localhost`.

By default all sites in bench will migrate

`MAINTENANCE_MODE` env variable pauses the scheduler and sets bench to maintainence mode before migration

```sh
docker exec -it \
    -e "SITES=site1.localhost:site2.localhost" \
    -e "MAINTENANCE_MODE=1" \
    erpnext_kubernetes_erpnext-python_1 docker-entrypoint.sh migrate
```

## Backup sites(s)

`SITES` env variable is list of sites separated by `:` to migrate. e.g. `SITES=site1.localhost` or `SITES=site1.localhost:site2.localhost`

By default all sites in bench will be backed up

`WITH_FILES` env variable backs up user uploaded files for the sites

```sh
docker exec -it \
    -e "SITES=site1.localhost:site2.localhost" \
    -e "WITH_FILES=1" \
    erpnext_kubernetes_erpnext-python_1 docker-entrypoint.sh backup
```
